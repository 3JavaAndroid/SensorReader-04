package com.mgabrynowicz.sensorreaderapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.ByteBuffer;

/**
 * Created by RENT on 2017-01-17.
 */

public class FileManager {
    private static final String FOLDER_NAME = "accelerometer";
    // nazwa pliku
    private final String fileName;

    // PrintWriter do zapisu danych w ascii na wyjście
    private PrintWriter printWriter;

    public FileManager() {
        // wygenerowanie - aktualny czas w milisekundach od 1970
        fileName = String.valueOf(System.currentTimeMillis());
    }

    // otwieramy plik
    public void openOutputFile() {
        String folderPath = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/" + FOLDER_NAME;
        checkAndCreateFolder(folderPath);

        try {
            FileOutputStream fos = new FileOutputStream(folderPath + "/" + fileName);
            printWriter = new PrintWriter(fos);
        } catch (IOException ioe) {
            Log.e(getClass().getName(), ioe.getMessage());
        }
    }

    // sprawdza czy folder o danej ścieżce istnieje, jeśli nie istnieje to go tworzy
    // sprawdza czy plik istnieje.
    // jeśli folder nie istnieje lub pod podaną ścieżką istnieje plik (a nie katalog) to tworzy katalog
    private void checkAndCreateFolder(String path) {
        File folderFile = new File(path);
        if (!folderFile.exists() || !folderFile.isDirectory()) {
            folderFile.mkdir();
        }
    }

    public void saveDataToFile(float x, float y, float z) throws Exception {
        StringBuilder builder = new StringBuilder();

        builder.append(x);
        builder.append(";");
        builder.append(y);
        builder.append(";");
        builder.append(z);

        printWriter.println(builder.toString());
    }

    public void closeOutputFile() {
        printWriter.close();
    }

}
